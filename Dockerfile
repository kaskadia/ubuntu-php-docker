# Set the base image
FROM ubuntu:18.04
# Dockerfile author / maintainer
MAINTAINER David Kartik <david.kartik@kaskadia.com>
RUN set -xe
# replace shell with bash so we can source files
RUN rm /bin/sh && ln -s /bin/bash /bin/sh
RUN touch ~/.bashrc && chmod +x ~/.bashrc
RUN apt-get update && apt-get install -y software-properties-common && add-apt-repository -y ppa:ondrej/php
RUN apt-get update -yqq 
RUN apt-get install -y curl gnupg 
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends apt-utils && DEBIAN_FRONTEND=noninteractive apt-get install git php7.4-fpm php7.4-mbstring php7.4-json php7.4-opcache php7.4-pgsql php7.4-xml php7.4-curl php7.4-cli php7.4-zip php7.4-ldap php7.4-xml php7.4-gd php7.4-intl php7.4-bcmath php7.4-bz2 php-dev libmcrypt-dev php-pear zip unzip postgresql-client -yqq
#RUN curl --silent -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.2/install.sh | bash
#ENV NVM_DIR /root/.nvm
#RUN source $NVM_DIR/nvm.sh \
#  && nvm install stable \
#  && nvm alias default stable \
#  && nvm use default \
#  && npm install -g yarn
RUN curl -sL https://deb.nodesource.com/setup_10.x -o nodesource_setup.sh && bash nodesource_setup.sh
RUN apt-get install -y nodejs
RUN npm install -g yarn

#RUN wget https://download.libsodium.org/libsodium/releases/libsodium-1.0.15.tar.gz && tar -xzvf libsodium-1.0.15.tar.gz && cd libsodium-1.0.15 && chmod +x configure && ./configure && make check && make install
RUN pecl channel-update pecl.php.net
RUN pecl install -f mcrypt-1.0.3
#RUN echo "extension=sodium.so" > /usr/local/etc/php/conf.d/libsodium.ini
RUN cd /usr/local/bin && curl -sS https://getcomposer.org/installer | php && mv composer.phar composer && cd /
#CMD [ "php-fpm" ]
